# init project
start:
	docker-compose up -d
	cd server && docker-compose exec php-fpm composer install
	cd client && docker-compose exec client npm install
	cd server && docker-compose exec php-fpm php bin/console d:s:u --force

# up project
up:
	docker-compose up -d --build

# down project
down:
	docker-compose down

# run project
run:
	docker-compose up

# install client/nodes_modules/ & server/vendor/
install:
	cd server && docker-compose exec php-fpm composer install
	cd client && docker-compose exec client npm install

# doctrine
dsu:
	cd server && docker-compose exec php-fpm php bin/console d:s:u --force

ddc:
	cd server && docker-compose exec php-fpm php bin/console d:d:c

ddd:
	cd server && docker-compose exec php-fpm php bin/console d:d:d --force
