import Vue from 'vue'
import App from './App.vue'
import router from "./routes/Router";
import VueRouter from "vue-router";
import store from "./vuex/store";

Vue.use(VueRouter);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  beforeCreate() {
    store.commit("initialiseStore");
  },
}).$mount('#app')
