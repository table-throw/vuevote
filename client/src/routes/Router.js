import VueRouter from "vue-router";
import Home from "../components/Home";
import HomeHeader from "../components/headers/HomeHeader";
import Login from "../components/Login";
import Register from "../components/Register";
import UserList from "../components/users/UserList";
import UserDetail from "../components/users/UserDetail";
import VoteList from "../components/votes/VoteList";
import VoteDetail from "../components/votes/VoteDetail";
import VoteForm from "../components/votes/VoteForm";
import store from "../vuex/store";

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/login",
            name: "Login",
            components: {
                default: Login
            }
        },
        {
            path: "/register",
            name: "Register",
            components: {
                default: Register
            }
        },
        {
            path: "/",
            name: "Home",
            components: {
                header: HomeHeader,
                default: Home
            },
            meta: {
                requiresAuth: true
            },
            requiresAuth: true
        },
        {
            path: "/users",
            name: "UserList",
            components: {
                header: HomeHeader,
                default: UserList
            },
            requiresAuth: true
        },
        {
            path: "/users/:id",
            name: "UserDetail",
            components: {
                header: HomeHeader,
                default: UserDetail
            },
            requiresAuth: true
        },
        {
            path: "/votes",
            name: "VoteList",
            components: {
                header: HomeHeader,
                default: VoteList
            },
            requiresAuth: true
        },
        {
            path: "/votes/create",
            name: "VoteForm",
            components: {
                header: HomeHeader,
                default: VoteForm
            },
            requiresAuth: true
        },
        {
            path: "/votes/:id",
            name: "VoteDetail",
            components: {
                header: HomeHeader,
                default: VoteDetail
            },
            requiresAuth: true
        }
    ]
});

router.beforeEach((to, from, next) => {
    console.log(store.getters.authToken)
    if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.authToken) {
        return router.push({ name: 'Login' });
    }

    return next();
})

export default router;
