import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import router from "../routes/Router";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        authToken: "",
        connectedUserId: 1
    },
    mutations: {
        addUser(state, user) {
            axios.post("http://127.0.0.1:1337/user", {
                "firstName": user.firstName,
                "lastName": user.lastName,
                "email": user.email,
                "password": user.password,
                "dateOfBirth": user.dateOfBirth
            });

        },
        addVote(state, vote) {
            axios.get("http://localhost:1337/user/" + state.connectedUserId).then(res => {
                axios.post("http://127.0.0.1:1337/vote", {
                    "title": vote.title,
                    "description": vote.description,
                    "startDate": vote.startDate,
                    "endDate": vote.endDate,
                    "votePour": { ids: [res.data.id] },
                    "voteContre": { ids: [] }
                });
            });

        },
        initialiseStore(state) {
            // Check if the ID exists
            if (localStorage.getItem('store')) {
                // Replace the state object with the stored item
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );
            }
        },
        logout(state) {
            state.authToken = ""
            state.connectedUserId = 0
            router.push({ name: "Login" })
        },
        saveConnectedUser(state, user) {
            state.authToken = user.data.token
            state.connectedUserId = user.data.id
        }
    },
    actions: {
        addUser: function ({ commit }, user) {
            commit('addUser', user)
        },
        addVote: function ({ commit }, vote) {
            commit('addVote', vote)
        },
        login: function ({ commit }, user) {
            axios.post("http://localhost:1337/login", {
                "email": user.email,
                "password": user.password,
            }).then(res => {
                console.log(res)
                if (res.status == 200) {
                    commit('saveConnectedUser', res)
                    router.push({ name: "Home" })
                }
            })
        },
        logout: function ({ commit }) {
            commit('logout')
        }
    },
    getters: {
        authToken: state => state.authToken,
        connectedUserId: state => state.connectedUserId,
    }
});

export default store
